<h1 align="center" id="title">Nice_UI</h1>

<p align="center"><img src="https://socialify.git.ci/vortexdevsoftware/Nice_ui/image?description=1&amp;font=Source%20Code%20Pro&amp;language=1&amp;logo=https%3A%2F%2Fraw.githubusercontent.com%2Fvortexdevsoftware%2FNice_ui%2Fmain%2Fmedia%2Flogo_modernized.png&amp;name=1&amp;owner=1&amp;theme=Dark" alt="project-image"></p>

<p align="center" id="description">A feature-rich single header C++ system for <a href="https://www.glfw.org/">GLFW</a></p>
<p align="center"><img src="https://img.shields.io/github/license/vortexdevsoftware/Nice_ui?color=lightgrey&style=for-the-badge" alt="shields"> <img src="https://img.shields.io/github/stars/vortexdevsoftware/Nice_ui?style=for-the-badge" alt="shields"> <img src="https://img.shields.io/github/forks/vortexdevsoftware/Nice_ui?color=yellow&amp;style=for-the-badge" alt="shields"></p>

<h2 align="center">💡 Features</h2>

<p align="center">Here are some of the project's best features:</p>

*   📝 Simple to use without any hidden surprises and focused towards expected behavior.
*   🧱 Object-Oriented architecture; This means there is no need for creating the same element once every single frame just design the interface before the main loop and use Context->Render() to draw it.
*   🖥️ DPI Aware interface this means that when properly using screen-size based size values your elements should scale properly to many other resolutions.
*   📀 GLSL Shader support, if you require spicing-up your interface for your project.
*   🗔 Easily extensible framework. See <a href="plugins">Plugins</a>.
