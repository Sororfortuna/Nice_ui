/**
 * @file docking.cpp
 * @author Igor Alexey Marengo (igor@vortex.com)
 * @brief This configurable extension for Nice_UI allows docking of Frames to all sides of the viewport 
 * @version 0.1
 * @date 2021-12-19
 * 
 * @copyright Copyright (c) 2021
 * 
 */

// Specify that we are getting only the declarations since we don't actually need any of the functionality that already comes with Nice_UI
// otherwise we would get a no-implementation warning.
#define NICE_UI_DECLARATIONS_ONLY
#include "nice_ui.h"

